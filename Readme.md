# Daikatana Maps

By the community, for the community

## Instructions

Please follow the following structural scheme:

+ `dk_mapname` for single-player maps
+ `dk_mp_namename` for multi-player maps
+ Write these in all lower-case characters
+ Make a directory for each map file which is of same name as the map itself

If you didn't pick the pre-built package, then you can clone this repository and do it on your own. Or you could fork the repository, make changes to existing maps and then do a pull request. If you don't know how to use Git, then you can learn from Bitbucket's and GitHub's documentation which is mostly fairly easy to comprehend and follow.

## List of availabl maps with descriptions

### Singleplayer

+ `dk_demo`
	* Demonstration map for this repository
	* Feel free to copy this as a base of your own map but don't make changes against this map
	* Episode 1

### Multiplayer

+ `dk_mp_demo`
	* Demonstation multiplayer map for this repository
	* You may copy this map to work as a template for your own map but don't make changes against this particular map
	* For 2-4 players
	* Episode 1

## Contributors

If you want your name here make a map.

